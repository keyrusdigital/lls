//
//  Animator.swift
//  LLS
//
//  Created by Gabriel Bremond on 04/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import Foundation
import UIKit

class Animator: NSObject, UIViewControllerAnimatedTransitioning {

    func transitionDuration(using context: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.4
    }

    func animateTransition(using context: UIViewControllerContextTransitioning) {
        guard let fromViewController = context.viewController(forKey: UITransitionContextViewControllerKey.from) else {
            return
        }
        guard let toViewController = context.viewController(forKey: UITransitionContextViewControllerKey.to) else {
            return
        }

        // Setup ignital values.
        let finalFrameForVC = context.finalFrame(for: toViewController)
        let containerView = context.containerView
        let bounds = UIScreen.main.bounds
        toViewController.view.frame = finalFrameForVC.offsetBy(dx: 0, dy: bounds.size.height)
        containerView.addSubview(toViewController.view)

        // Animate destination frame.
        UIView.animate(withDuration: transitionDuration(using: context), delay: 0.0,
                       usingSpringWithDamping: 0.5, initialSpringVelocity: 0.0,
                       options: .curveEaseInOut, animations: {
                        fromViewController.view.alpha = 0.5
                        toViewController.view.frame = finalFrameForVC
        }, completion: { _ in
            context.completeTransition(true)
            fromViewController.view.alpha = 1.0
        })
    }
}
