//
//  ChapterTableViewCell.swift
//  LLS
//
//  Created by Gabriel Bremond on 03/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import UIKit

class ChapterTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!

    @IBOutlet weak var picture: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    /// Methods to update a chapter cell with a chapter.
    ///
    /// - Parameters:
    ///   - chapter: Chapter to update against.
    func update(_ chapter: Chapter) {
        title.text = chapter.title

        if let theURL = chapter.url {
            if let URL = URL(string: theURL) {
                WebserviceConsummer().downloadImage(url: URL) { (image) in
                    self.picture.image = image
                }
            }
        }

        if chapter.valid {
            title.backgroundColor = #colorLiteral(red: 0.03187843412, green: 0.4863227606, blue: 0.7310805917, alpha: 1)
            picture.alpha = 0.4
            isUserInteractionEnabled = true
        } else {
            title.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            picture.alpha = 0.1
            isUserInteractionEnabled = false
        }
    }

}
