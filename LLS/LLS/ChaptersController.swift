//
//  ChaptersController.swift
//  LLS
//
//  Created by Gabriel Bremond on 03/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import UIKit

class ChaptersController: UITableViewController {

    private let bookID = Bundle.main.object(forInfoDictionaryKey: "BookID") as? String

    private var chapters: [Chapter] {
        // Build chapters list.
        if let book = self.bookID {
            let predicateFormat: String = String(format: "book = '%@'", book)
            let predicate: NSPredicate = NSPredicate(format:predicateFormat)
            return Chapter.list(byPredicate: predicate)
        }

        return []
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = NSLocalizedString("Chapters", comment: "")

        self.refreshControl?.addTarget(self, action: #selector(reloadChapters), for: UIControlEvents.valueChanged)

        reloadChapters()
    }

    // Webservice loading.
    internal func reloadChapters() {
        if let book = bookID {
            // Display a loading indicator.
            UIApplication.shared.isNetworkActivityIndicatorVisible = true

            WebserviceConsummer().getElements(type: .chapters, parentID: book, completionHandler: { (data) in
                defer {
                    Thread.performOnMain {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        self.refreshControl?.endRefreshing()
                    }
                }

                // Treat returned data.
                if let returnedData = data {
                    ChaptersHelper.load(data: returnedData)
                    Thread.performOnMain {
                        self.tableView.reloadData()
                    }
                }
            })
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chapters.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Chapter", for: indexPath)
            as? ChapterTableViewCell else {
                return UITableViewCell()
        }

        let chapter = chapters[indexPath.row]
        cell.update(chapter)

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row >= 0 && indexPath.row < chapters.count else {
            return
        }

        let chapter = chapters[indexPath.row]

        // Allow selection only on valid chapters.
        if chapter.valid {
            if let lessons = storyboard?.instantiateViewController(withIdentifier: "LessonsVC") as? LessonsController {
                lessons.chapter = chapter
                navigationController?.pushViewController(lessons, animated: true)
            }
        }
    }

}
