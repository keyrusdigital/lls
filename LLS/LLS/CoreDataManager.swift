//
//  CoreDataManager.swift
//  LLS
//
//  Created by Gabriel Bremond on 03/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import Foundation
import CoreData
import UIKit

let runSQLitePath: String = "lls.db"
let testSQLitePath: String = "test_lls.db"

/// Core data helper that manages the whole core data stack
class CoreDataManager: NSObject {
    static let shared: CoreDataManager = CoreDataManager()

    private var _managedObjectContext: NSManagedObjectContext?

    /// Run mode to switch the database file
    static var runMode: Bool = true {
        willSet {
            shared.removeDatabase()
        }
    }

    var managedObjectModel: NSManagedObjectModel?
    var persistentStoreCoordinator: NSPersistentStoreCoordinator?
    var managedObjectContext: NSManagedObjectContext? {
        get {
            if self.isStackInitialized() {
                return _managedObjectContext
            }

            _managedObjectContext = self.createNewContext()
            return _managedObjectContext
        }
        set {
            _managedObjectContext = newValue
        }
    }

    // MARK: - Private instance methods

    fileprivate override init() {
        guard let managedOM = NSManagedObjectModel.mergedModel(from: nil) else {
            return
        }

        managedObjectModel = managedOM
        self.persistentStoreCoordinator = NSPersistentStoreCoordinator.init(
            managedObjectModel: managedOM)
    }

    /// Check if the core data stack has already been initialized
    /// - return : true if the stack is ready, false otherwise
    fileprivate func isStackInitialized() -> Bool {
        guard let context = _managedObjectContext,
            let coordinator = self.persistentStoreCoordinator
            else {
                return false
        }

        return coordinator != context.persistentStoreCoordinator || !coordinator.persistentStores.isEmpty
    }

    /// Create a new context when the stack needs to be initialized or if the store doesn't exist anymore
    /// - return : the create managed context
    func createNewContext() -> NSManagedObjectContext? {
        var context: NSManagedObjectContext? = nil

        if self.persistentStoreCoordinator?.persistentStores.isEmpty ?? false {
            do {
                try self.persistentStoreCoordinator?.addPersistentStore(ofType:NSSQLiteStoreType,
                                                                        configurationName:nil,
                                                                        at:self.storeUrl(),
                                                                        options:nil)
            } catch {
                NSLog("Unable to create persistent coordinator")
            }
        }

        if let coordinator = self.persistentStoreCoordinator {
            context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            context?.persistentStoreCoordinator = coordinator
        }

        return context
    }

    /// Get the url of the core data database depending on the running mode
    /// - return : the url of the database
    func storeUrl() -> URL? {
        let sqlitePath: String = CoreDataManager.runMode ? runSQLitePath : testSQLitePath
        let directory: String? = NSSearchPathForDirectoriesInDomains(
            FileManager.SearchPathDirectory.documentDirectory,
            FileManager.SearchPathDomainMask.userDomainMask, true).last
        guard let dir = directory else {
            return nil
        }

        return NSURL.fileURL(withPath: "\(dir)/\(sqlitePath)")
    }

    /// Remove the database and the store which depends on it
    /// - return : true if the database has been successfuly removed
    @discardableResult func removeDatabase() -> Bool {
        guard let stores: [NSPersistentStore] = self.persistentStoreCoordinator?.persistentStores else {
            NSLog("No stores to remove")
            return true
        }

        do {
            for store: NSPersistentStore in stores {
                try self.persistentStoreCoordinator!.remove(store)
                try FileManager.default.removeItem(atPath:self.storeUrl()!.path)
            }
            _managedObjectContext = nil
        } catch {
            NSLog("Can't remove store")
            return false
        }

        return true
    }

    // MARK: - Public static methods

    /// Get the unique managed context of the application
    /// - return : the managed context
    static func managedContext() -> NSManagedObjectContext? {
        return shared.managedObjectContext
    }

    /// Static function to remove the database
    /// - return : the custom font with the right size
    @discardableResult static func removeDatabase() -> Bool {
        return shared.removeDatabase()
    }
}
