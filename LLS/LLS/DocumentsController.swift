//
//  DocumentsController.swift
//  LLS
//
//  Created by Gabriel Bremond on 03/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import UIKit

class DocumentsController: UIViewController {

    /// Lesson to load documments from.
    var lesson: Lesson?

    private var documents: [Document] {
        if let documents = lesson?.documents?.allObjects as? [Document] {
            return documents
        }

        return []
    }

    @IBOutlet weak var webview: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = lesson?.title

        reloadTemplate()
    }

    // Webservice loading.
    internal func reloadTemplate() {
        if let lessonID = lesson?.identifier {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true

            WebserviceConsummer().getElements(type: .template, parentID: String(lessonID),
                                              completionHandler: { (data) in
                defer {
                    Thread.performOnMain {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
                }

                // Treat returned data.
                if let returnedData = data {
                    DocumentsHelper.load(data: returnedData)
                    if let HTMLString = DocumentsHelper.htmlRepresentation(documents: self.documents) {
                        self.webview.loadHTMLString(HTMLString, baseURL: nil)
                    }
                }
            })
        }
    }
}
