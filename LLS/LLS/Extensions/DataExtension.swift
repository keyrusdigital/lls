//
//  DataExtension.swift
//  LLS
//
//  Created by Gabriel Bremond on 03/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import Foundation

extension Data {

    /// Save data into cache directory.
    ///
    /// - Parameters:
    ///   - name: File name.
    func saveToTemp(name: String) {
        let fileManager = FileManager.default
        let fullpath = NSTemporaryDirectory() + name
        fileManager.createFile(atPath: fullpath, contents: self, attributes: nil)
    }

    /// Load data from cache directory.
    ///
    /// - Parameters:
    ///   - name: File name.
    /// - return : Data if found, nil otherwise.
    static func loadFromTemp(name: String) -> Data? {
        let fullpath = NSTemporaryDirectory() + name

        if let file: FileHandle = FileHandle(forReadingAtPath: fullpath) {
            // Read all the data
            let data = file.readDataToEndOfFile()

            // Close the file
            file.closeFile()

            return data
        }

        return nil
    }
}
