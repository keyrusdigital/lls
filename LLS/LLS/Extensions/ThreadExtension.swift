//
//  ThreadExtension.swift
//  LLS
//
//  Created by Gabriel Bremond on 03/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import Foundation

extension Thread {

    /// Perform closure into main thread.
    ///
    /// - Parameters:
    ///   - closure: Code to perform in main thread.
    static func performOnMain(_ closure: () -> Void) {
        if Thread.current.isMainThread {
            closure()
        } else {
            DispatchQueue.main.sync {
                closure()
            }
        }
    }
}
