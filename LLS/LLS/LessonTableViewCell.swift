//
//  LessonTableViewCell.swift
//  LLS
//
//  Created by Gabriel Bremond on 03/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import UIKit

class LessonTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!

    @IBOutlet weak var type: UILabel!

    @IBOutlet weak var page: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    /// Methods to update a lesson cell with a lesson.
    ///
    /// - Parameters:
    ///   - lesson: Lesson to update against.
    func update(_ lesson: Lesson) {
        title.text = lesson.title
        type.text = lesson.type
        page.text = String(lesson.page)

        if lesson.valid {
            title.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            type.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            page.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            isUserInteractionEnabled = true
        } else {
            title.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            type.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            page.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            isUserInteractionEnabled = false
        }
    }
}
