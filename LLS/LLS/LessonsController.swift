//
//  LessonsController.swift
//  LLS
//
//  Created by Gabriel Bremond on 03/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import UIKit

class LessonsController: UITableViewController {

    /// The chapter to load lessons from.
    var chapter: Chapter?

    private var lessons: [Lesson] {
        // Navigation rely on the lessons.
        updateNavigationButtons()

        // Build lessons list (ordered by page number)
        if let lessons = chapter?.lessons?.allObjects as? [Lesson] {
            let sortedLessons = lessons.sorted { (lessonA, lessonB) -> Bool in
                return lessonA.page < lessonB.page
            }

            return sortedLessons
        }

        return []
    }

    private var navigationCell: NavigationTableViewCell?

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = chapter?.title

        reloadChapters()
    }

    // Webservice loading.
    internal func reloadChapters() {
        if let chapterID = chapter?.identifier {
            // Display a loading indicator.
            UIApplication.shared.isNetworkActivityIndicatorVisible = true

            WebserviceConsummer().getElements(
                type: .lessons, parentID: String(chapterID), completionHandler: { (data) in
                    defer {
                        Thread.performOnMain {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                    }

                    // Treat returned data.
                    if let returnedData = data {
                        LessonsHelper.load(data: returnedData)
                        Thread.performOnMain {
                            self.navigationItem.title = self.chapter?.title
                            self.tableView.reloadData()
                        }
                    }
            })
        }
    }

    // Navigation elements update.
    internal func updateNavigationButtons() {
        guard let navCell = navigationCell else {
            return
        }

        // If could have no chapter.
        guard let currentChapter = chapter else {
            navCell.previousButton.isHidden = true
            navCell.nextButton.isHidden = true
            return
        }

        navCell.previousButton.isHidden = false
        navCell.nextButton.isHidden = false

        if ChaptersHelper.previousChapter(chapter: currentChapter) == nil {
            navCell.previousButton.isHidden = true
        }
        if ChaptersHelper.nextChapter(chapter: currentChapter) == nil {
            navCell.nextButton.isHidden = true
        }
    }

    // MARK: - Navigation actions

    @IBAction func previous(_ sender: UIButton) {
        guard let currentChapter = chapter else {
            return
        }

        if let previousChapter = ChaptersHelper.previousChapter(chapter: currentChapter) {
            chapter = previousChapter
            Thread.performOnMain {
                self.tableView.reloadData()
            }
            reloadChapters()
        }
    }

    @IBAction func next(_ sender: UIButton) {
        guard let currentChapter = chapter else {
            return
        }

        if let nextChapter = ChaptersHelper.nextChapter(chapter: currentChapter) {
            chapter = nextChapter
            Thread.performOnMain {
                self.tableView.reloadData()
            }
            reloadChapters()
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lessons.count + 1
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 50
        }

        return 100
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "Navigation", for: indexPath)
                as? NavigationTableViewCell else {
                    return UITableViewCell()

            }

            navigationCell = cell

            return cell
        }

        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Lesson", for: indexPath)
            as? LessonTableViewCell else {
                return UITableViewCell()
        }

        let lesson = lessons[indexPath.row - 1]
        cell.update(lesson)

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row > 0 && indexPath.row < (lessons.count + 1) else {
            return
        }

        let lesson = lessons[indexPath.row - 1]

        // Allow selection only on valid lessons.
        if lesson.valid {
            if let template = storyboard?.instantiateViewController(withIdentifier: "TemplateVC")
                as? DocumentsController {
                template.lesson = lesson
                navigationController?.pushViewController(template, animated: true)
            }
        }
    }

}
