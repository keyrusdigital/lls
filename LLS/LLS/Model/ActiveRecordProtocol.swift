//
//  ActiveRecordProtocol.swift
//  LLS
//
//  Created by Gabriel Bremond on 03/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import Foundation

/// Protocol that defines all methods which must be implemented to build the active record pattern
protocol ActiveRecordProtocol {
    associatedtype TYP

    func save()
    func delete()

    static func create() -> TYP
    static func list() -> [TYP]
    static func list(byPredicate predicate: NSPredicate?) -> [TYP]
}
