//
//  ChapterExtension.swift
//  LLS
//
//  Created by Gabriel Bremond on 03/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import Foundation
import CoreData

extension Chapter {

    /// Chapter loader.
    ///
    /// - Parameters:
    ///   - chapterDictionary: Dictionary representation.
    func load(chapterDictionary: [String: Any]) {
        guard chapterDictionary.count > 0 else {
            return
        }

        if let anIdentifier = chapterDictionary["id"] as? Int {
            identifier = Int64(anIdentifier)
        }
        if let aBook = chapterDictionary["book"] as? Int {
            book = Int64(aBook)
        }
        if let aColorTheme = chapterDictionary["colorTheme"] as? String {
            colorTheme = aColorTheme
        }
        if let theLessons = chapterDictionary["lessons"] as? [Int64] {
            lessons = buildLessons(ids: theLessons)
        }
        if let theShowNumber = chapterDictionary["numShow"] as? Int {
            showNumber = Int64(theShowNumber)
        }
        if let aTheme = chapterDictionary["theme"] as? String {
            theme = aTheme
        }
        if let aTitle = chapterDictionary["title"] as? String {
            title = aTitle
        }
        if let aURL = chapterDictionary["url"] as? String {
            url = aURL
        }
        if let validity = chapterDictionary["valid"] as? Bool {
            valid = validity
        }
    }

    private func buildLessons(ids: [Int64]) -> NSSet {
        let lessonList: NSSet = NSSet()
        for lessonID in ids {
            if let existingLesson = Lesson.find(byIdServer: Int64(lessonID)) {
                existingLesson.identifier = Int64(lessonID)
                lessonList.adding(existingLesson)
            } else {
                let newLesson = Lesson.create()
                newLesson.identifier = Int64(lessonID)
                newLesson.save()
                lessonList.adding(newLesson)
            }
        }

        return lessonList
    }
}

// MARK: - Active Record Protocol Extension

/// Extension which manages the core data queries
extension Chapter: ActiveRecordProtocol {
    typealias TYP = Chapter
    static let queue: DispatchQueue = DispatchQueue(label: "fr.photograve.chaptersQueue", attributes: .concurrent)

    /// Save the chapter in the core data database
    func save() {
        Chapter.queue.sync {
            do {
                if let context = CoreDataManager.managedContext() {
                    try context.save()
                }
            } catch {
                NSLog("Unable to save object")
            }
        }
    }

    /// Delete the chapter
    func delete() {
        Chapter.queue.sync {
            if let context = CoreDataManager.managedContext() {
                context.delete(self)
            }
        }
    }

    /// Delete all chapters
    static func deleteAll() {
        let chapters: [Chapter] = Chapter.list()

        for chapter in chapters {
            chapter.delete()
        }
    }

    /// Create a core data managed chapter (used instead of init() which leads to a crash)
    static func create() -> Chapter {
        if let context = CoreDataManager.managedContext() {
            if #available(iOS 10.0, *) {
                return Chapter(context: context)
            } else {
                guard let entityDesc = NSEntityDescription.entity(forEntityName: "Chapter", in: context) else {
                    return Chapter()
                }

                return Chapter(entity: entityDesc, insertInto: context)
            }
        }
        return Chapter()
    }

    /// List all chapters stored in core data
    /// - return : a list of all chapters
    static func list() -> [Chapter] {
        return list(byPredicate: nil)
    }

    /// List chapters by an predicate
    /// - parameters :
    ///   - prediacte : the predicate which includes the where clause
    /// - return : the list of filtered chapters
    static func list(byPredicate predicate: NSPredicate?) -> [Chapter] {
        var allResults: [Chapter] = []

        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Chapter")

        if let aPredicate = predicate {
            fetchRequest.predicate = aPredicate
        }

        Chapter.queue.sync {
            do {
                if let context = CoreDataManager.managedContext() {
                    let results = try context.fetch(fetchRequest)

                    if let objects = results as? [Chapter] {
                        allResults = objects
                    }
                }
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
            }
        }

        return allResults
    }

    /// Find chapter by its server identifier (used to update a chapter on a web service response)
    /// - parameters :
    ///   - identifiant : th server identifier for a chapter
    /// - return : the chapter found
    static func find(byIdServer identifiant: Int64) -> Chapter? {
        let predicateFormat: String = String(format: "identifier = '%i'", identifiant)
        let predicate: NSPredicate = NSPredicate(format:predicateFormat)

        let chapters: [Chapter] = list(byPredicate: predicate)

        if chapters.isEmpty {
            return nil
        }
        return chapters[0]
    }
}
