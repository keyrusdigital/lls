//
//  ChaptersHelper.swift
//  LLS
//
//  Created by Gabriel Bremond on 03/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import Foundation

class ChaptersHelper: NSObject {

    /// Chapters loader.
    ///
    /// - Parameters:
    ///   - data: JSON Data representation.
    static func load(data: Data) {
        // Test entrie.
        if data.isEmpty {
            return
        }

        // Parse json data.
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: [])

            // Build list of Chapters.
            if let collection = json as? [Any] {

                // Remove chapters cache.
                Chapter.deleteAll()

                for object in collection {
                    // Supposed to load a collection of dictionary.
                    guard let dic = object as? [String: Any] else {
                        continue
                    }

                    // Supposed to have an identifier.
                    if (dic["id"] as? Int) == nil {
                        continue
                    }

                    let chapter = Chapter.create()
                    chapter.load(chapterDictionary: dic)
                    chapter.save()
                }
            }
        } catch let error {
            print(error)
        }
    }

    /// Find previous chapters.
    ///
    /// - Parameters:
    ///   - chapter: Next chapter.
    /// - return : Previous chapter (can be nil)
    static func previousChapter(chapter: Chapter) -> Chapter? {
        let chapters = Chapter.list()
        guard let index = chapters.index(of: chapter) else {
            return nil
        }

        if index <= 0 {
            return nil
        }

        return chapters[index - 1]
    }

    /// Find next chapter.
    ///
    /// - Parameters:
    ///   - chapter: Previous chapter.
    /// - return : Next chapter (can be nil)
    static func nextChapter(chapter: Chapter) -> Chapter? {
        let chapters = Chapter.list()
        guard let index = chapters.index(of: chapter) else {
            return nil
        }

        if index >= (chapters.count - 1) {
            return nil
        }

        return chapters[index + 1]
    }

    /// Retrieve only valid chapters.
    ///
    /// - return : List of valid chapters (can be empty)
    static func validChapters() -> [Chapter] {
        var valids: [Chapter] = []

        for chapter in Chapter.list() where chapter.valid {
            valids.append(chapter)
        }

        return valids
    }
}
