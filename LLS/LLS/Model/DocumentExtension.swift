//
//  DocumentExtension.swift
//  LLS
//
//  Created by Gabriel Bremond on 03/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import Foundation
import CoreData

protocol DocumentProtocol {
    /// Build HTML string representation.
    ///
    /// - return : HTML string.
    func htmlRepresentation() -> String?
}

extension Document: DocumentProtocol {

    /// Document loader.
    ///
    /// - Parameters:
    ///   - templateDictionary: Dictionary representation.
    func load(templateDictionary: [String: Any]) {
        guard templateDictionary.count > 0 else {
            return
        }

        if let anIdentifier = templateDictionary["id"] as? Int {
            identifier = Int64(anIdentifier)
        }
        if let anOrder = templateDictionary["order"] as? Int {
            order = Int64(anOrder)
        }
        if let aLesson = templateDictionary["lessonId"] as? Int {
            if let foundLesson = Lesson.find(byIdServer: Int64(aLesson)) {
                foundLesson.documents?.adding(self)
                lesson = foundLesson
            } else {
                let newLesson = Lesson.create()
                newLesson.identifier = Int64(aLesson)
                newLesson.save()
                lesson = newLesson
            }
        }
    }

    // MARK: - Document protocol

    func htmlRepresentation() -> String? {
        return nil
    }
}
