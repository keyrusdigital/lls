//
//  DocumentsHelper.swift
//  LLS
//
//  Created by Gabriel Bremond on 03/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import Foundation

class DocumentsHelper {

    /// Template loader.
    ///
    /// - Parameters:
    ///   - data: JSON Data representation.
    static func load(data: Data) {
        // Test entrie.
        if data.isEmpty {
            return
        }

        // Parse json data.
        do {
            if let templateDictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any] {
                retriveElements(dic: templateDictionary)
            }
        } catch let error {
            print(error)
        }
    }

    private static func retriveElements(dic: [String: Any]) {
        guard dic.count > 0 else {
            return
        }

        guard let lessonDic = dic["lesson"] as? [String: Any] else {
            return
        }

        guard let lessonId = lessonDic["id"] as? Int else {
            return
        }

        if let theDocuments = dic["documents"] as? [Any] {
            // Clear documents cache.
            ImageDocument.deleteAll()
            TextDocument.deleteAll()

            // Build list of documents.
            retrieveDocuments(theDocuments, lessonId)
        }
    }

    private static func retrieveDocuments(_ list: [Any], _ lessonId: Int) {
        for item in list {
            guard let docDic = item as? [String: Any] else {
                continue
            }

            var mutableDocDic = docDic
            mutableDocDic["lessonId"] = lessonId

            if let type = docDic["_type"] as? String,
                let identifier = docDic["id"] as? Int {
                switch type {
                case "Text":
                    if let existingTextDoc = TextDocument.find(byIdServer: Int64(identifier)) {
                        existingTextDoc.load(templateDictionary: mutableDocDic)
                        existingTextDoc.save()
                    } else {
                        let newTextDoc = TextDocument.create()
                        newTextDoc.load(templateDictionary: mutableDocDic)
                        newTextDoc.save()
                    }
                    break
                case "Image":
                    if let existingImageDoc = ImageDocument.find(byIdServer: Int64(identifier)) {
                        existingImageDoc.load(templateDictionary: mutableDocDic)
                        existingImageDoc.save()
                    } else {
                        let newImageDoc = ImageDocument.create()
                        newImageDoc.load(templateDictionary: mutableDocDic)
                        newImageDoc.save()
                    }
                    break
                default:
                    break
                }
            }
        }
    }

    // MARK: - Document protocol

    static func htmlRepresentation(documents: [Document]) -> String? {
        var htmlString = "<html><head>"
        htmlString.append("<meta name=\"viewport\" ")
        htmlString.append("content=\"width=device-width, initial-scale=1.0, user-scalable = no\"")
        htmlString.append(">")
        htmlString.append("</head><body>")
        for document in documents {
            if let content = document.htmlRepresentation() {
                htmlString.append(content)
            }
        }
        htmlString.append("</body></html>")

        return htmlString
    }
}
