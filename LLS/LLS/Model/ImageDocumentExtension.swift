//
//  ImageDocumentExtension.swift
//  LLS
//
//  Created by Gabriel Bremond on 03/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import Foundation
import CoreData

extension ImageDocument {
    override func load(templateDictionary: [String: Any]) {
        super.load(templateDictionary: templateDictionary)

        guard templateDictionary.count > 0 else {
            return
        }

        if let aURL = templateDictionary["url"] as? String {
            url = aURL
        }
        if let aCredits = templateDictionary["credits"] as? String {
            credits = aCredits
        }
        if let aCaption = templateDictionary["caption"] as? String {
            caption = aCaption
        }
    }

    // MARK: - Document protocol

    override func htmlRepresentation() -> String? {
        // Always use secure connection.
        if let content = url?.replacingOccurrences(of: "http://", with: "https://") {
            var htmlString = "<p>"
            if let caption = caption {
                htmlString.append("\(caption)</br>")
            }
            htmlString.append("<img src=\(content) width=\"100%\"></img>")
            if let credits = credits {
                htmlString.append("</br>\(credits)")
            }
            htmlString.append("</p>")

            return htmlString
        }

        return nil
    }

}

// MARK: - Active Record Protocol Extension

/// Extension which manages the core data queries
extension ImageDocument: ActiveRecordProtocol {
    typealias TYP = ImageDocument
    static let queue: DispatchQueue = DispatchQueue(label: "fr.photograve.imageDocumentQueue", attributes: .concurrent)

    /// Save the image document in the core data database
    func save() {
        ImageDocument.queue.sync {
            do {
                if let context = CoreDataManager.managedContext() {
                    try context.save()
                }
            } catch {
                NSLog("Unable to save object")
            }
        }
    }

    /// Delete the image document
    func delete() {
        ImageDocument.queue.sync {
            if let context = CoreDataManager.managedContext() {
                context.delete(self)
            }
        }
    }

    /// Delete all image documents
    static func deleteAll() {
        let imageDocs: [ImageDocument] = ImageDocument.list()

        for imageDoc in imageDocs {
            imageDoc.delete()
        }
    }

    /// Create a core data managed image document (used instead of init() which leads to a crash)
    static func create() -> ImageDocument {
        if let context = CoreDataManager.managedContext() {
            if #available(iOS 10.0, *) {
                return ImageDocument(context: context)
            } else {
                guard let entityDesc = NSEntityDescription.entity(forEntityName: "ImageDocument", in: context) else {
                    return ImageDocument()
                }

                return ImageDocument(entity: entityDesc, insertInto: context)
            }
        }
        return ImageDocument()
    }

    /// List all image documents stored in core data
    /// - return : a list of all image documents
    static func list() -> [ImageDocument] {
        return list(byPredicate: nil)
    }

    /// List image documents by an predicate
    /// - parameters :
    ///   - prediacte : the predicate which includes the where clause
    /// - return : the list of filtered image documents
    static func list(byPredicate predicate: NSPredicate?) -> [ImageDocument] {
        var allResults: [ImageDocument] = []

        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ImageDocument")

        if let aPredicate = predicate {
            fetchRequest.predicate = aPredicate
        }

        ImageDocument.queue.sync {
            do {
                if let context = CoreDataManager.managedContext() {
                    let results = try context.fetch(fetchRequest)

                    if let objects = results as? [ImageDocument] {
                        allResults = objects
                    }
                }
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
            }
        }

        return allResults
    }

    /// Find image document by its server identifier (used to update a image document on a web service response)
    /// - parameters :
    ///   - identifiant : th server identifier for a image document
    /// - return : the image document found
    static func find(byIdServer identifiant: Int64) -> ImageDocument? {
        let predicateFormat: String = String(format: "identifier = '%i'", identifiant)
        let predicate: NSPredicate = NSPredicate(format:predicateFormat)

        let imageDocs: [ImageDocument] = list(byPredicate: predicate)

        if imageDocs.isEmpty {
            return nil
        }
        return imageDocs[0]
    }
}
