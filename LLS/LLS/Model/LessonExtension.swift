//
//  LessonExtension.swift
//  LLS
//
//  Created by Gabriel Bremond on 03/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import Foundation
import CoreData

extension Lesson {

    /// Lesson loader.
    ///
    /// - Parameters:
    ///   - lessonDictionary: Dictionary representation.
    func load(lessonDictionary: [String: Any]) {
        guard lessonDictionary.count > 0 else {
            return
        }

        if let anIdentifier = lessonDictionary["id"] as? Int {
            identifier = Int64(anIdentifier)
        }
        if let aBook = lessonDictionary["book"] as? Int {
            book = Int64(aBook)
        }
        if let aChapter = lessonDictionary["chapter"] as? Int {
            if let foundChapter = Chapter.find(byIdServer: Int64(aChapter)) {
                chapter = foundChapter
            } else {
                let newChapter = Chapter.create()
                newChapter.identifier = Int64(aChapter)
                newChapter.save()
                chapter = newChapter
            }
        }
        if let aType = lessonDictionary["type"] as? String {
            type = aType
        }
        if let aPage = lessonDictionary["page"] as? Int {
            page = Int64(aPage)
        }
        if let aTitle = lessonDictionary["title"] as? String {
            title = aTitle
        }
        if let validity = lessonDictionary["valid"] as? Bool {
            valid = validity
        }
    }
}

// MARK: - Active Record Protocol Extension

/// Extension which manages the core data queries
extension Lesson: ActiveRecordProtocol {
    typealias TYP = Lesson
    static let queue: DispatchQueue = DispatchQueue(label: "fr.photograve.lessonsQueue", attributes: .concurrent)

    /// Save the lesson in the core data database
    func save() {
        Lesson.queue.sync {
            do {
                if let context = CoreDataManager.managedContext() {
                    try context.save()
                }
            } catch {
                NSLog("Unable to save object")
            }
        }
    }

    /// Delete the lesson
    func delete() {
        Lesson.queue.sync {
            if let context = CoreDataManager.managedContext() {
                context.delete(self)
            }
        }
    }

    /// Delete all lessons
    static func deleteAll() {
        let lessons: [Lesson] = Lesson.list()

        for lesson in lessons {
            lesson.delete()
        }
    }

    /// Create a core data managed lesson (used instead of init() which leads to a crash)
    static func create() -> Lesson {
        if let context = CoreDataManager.managedContext() {
            if #available(iOS 10.0, *) {
                return Lesson(context: context)
            } else {
                guard let entityDesc = NSEntityDescription.entity(forEntityName: "Lesson", in: context) else {
                    return Lesson()
                }

                return Lesson(entity: entityDesc, insertInto: context)
            }
        }
        return Lesson()
    }

    /// List all lessons stored in core data
    /// - return : a list of all lessons
    static func list() -> [Lesson] {
        return list(byPredicate: nil)
    }

    /// List lessons by an predicate
    /// - parameters :
    ///   - prediacte : the predicate which includes the where clause
    /// - return : the list of filtered lessons
    static func list(byPredicate predicate: NSPredicate?) -> [Lesson] {
        var allResults: [Lesson] = []

        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Lesson")

        if let aPredicate = predicate {
            fetchRequest.predicate = aPredicate
        }

        Lesson.queue.sync {
            do {
                if let context = CoreDataManager.managedContext() {
                    let results = try context.fetch(fetchRequest)

                    if let objects = results as? [Lesson] {
                        allResults = objects
                    }
                }
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
            }
        }

        return allResults
    }

    /// Find lesson by its server identifier (used to update a lesson on a web service response)
    /// - parameters :
    ///   - identifiant : th server identifier for a lesson
    /// - return : the lesson found
    static func find(byIdServer identifiant: Int64) -> Lesson? {
        let predicateFormat: String = String(format: "identifier = '%i'", identifiant)
        let predicate: NSPredicate = NSPredicate(format:predicateFormat)

        let lessons: [Lesson] = list(byPredicate: predicate)

        if lessons.isEmpty {
            return nil
        }
        return lessons[0]
    }
}
