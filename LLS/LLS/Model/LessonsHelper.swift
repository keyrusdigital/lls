//
//  LessonsHelper.swift
//  LLS
//
//  Created by Gabriel Bremond on 03/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import Foundation

class LessonsHelper: NSObject {

    /// Lessons loader.
    ///
    /// - Parameters:
    ///   - data: JSON Data representation.
    static func load(data: Data) {
        // Test entrie.
        if data.isEmpty {
            return
        }

        // Parse json data.
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: [])

            // Build list of lessons.
            if let collection = json as? [Any] {

                // Remove chapters cache.
                Lesson.deleteAll()

                for object in collection {
                    // Supposed to load a collection of dictionary.
                    guard let dic = object as? [String: Any] else {
                        continue
                    }

                    // Supposed to have an identifier.
                    if (dic["id"] as? Int) == nil {
                        continue
                    }

                    let lesson = Lesson.create()
                    lesson.load(lessonDictionary: dic)
                    lesson.save()
                }
            }
        } catch let error {
            print(error)
        }
    }

    /// Retrieve only valid lessons.
    ///
    /// - return : List of valid lessons (can be empty)
    static func validLessons() -> [Lesson] {
        var valids: [Lesson] = []

        for lesson in Lesson.list() where lesson.valid {
            valids.append(lesson)
        }

        return valids
    }
}
