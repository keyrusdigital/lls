//
//  TextDocumentExtension.swift
//  LLS
//
//  Created by Gabriel Bremond on 03/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import Foundation
import CoreData

extension TextDocument {

    override func load(templateDictionary: [String: Any]) {
        super.load(templateDictionary: templateDictionary)

        guard templateDictionary.count > 0 else {
            return
        }

        if let aContent = templateDictionary["content"] as? String {
            content = aContent
        }
    }

    // MARK: - Document protocol

    override func htmlRepresentation() -> String? {
        if let aContent = content {
            return "<p>\(aContent)</p>"
        }

        return nil
    }
}

// MARK: - Active Record Protocol Extension

/// Extension which manages the core data queries
extension TextDocument: ActiveRecordProtocol {
    typealias TYP = TextDocument
    static let queue: DispatchQueue = DispatchQueue(label: "fr.photograve.textDocumentQueue", attributes: .concurrent)

    /// Save the text document in the core data database
    func save() {
        TextDocument.queue.sync {
            do {
                if let context = CoreDataManager.managedContext() {
                    try context.save()
                }
            } catch {
                NSLog("Unable to save object")
            }
        }
    }

    /// Delete the text document
    func delete() {
        TextDocument.queue.sync {
            if let context = CoreDataManager.managedContext() {
                context.delete(self)
            }
        }
    }

    /// Delete all text documents
    static func deleteAll() {
        let textDocs: [TextDocument] = TextDocument.list()

        for textDoc in textDocs {
            textDoc.delete()
        }
    }

    /// Create a core data managed text document (used instead of init() which leads to a crash)
    static func create() -> TextDocument {
        if let context = CoreDataManager.managedContext() {
            if #available(iOS 10.0, *) {
                return TextDocument(context: context)
            } else {
                guard let entityDesc = NSEntityDescription.entity(forEntityName: "TextDocument", in: context) else {
                    return TextDocument()
                }

                return TextDocument(entity: entityDesc, insertInto: context)
            }
        }
        return TextDocument()
    }

    /// List all text documents stored in core data
    /// - return : a list of all text documents
    static func list() -> [TextDocument] {
        return list(byPredicate: nil)
    }

    /// List text documents by an predicate
    /// - parameters :
    ///   - prediacte : the predicate which includes the where clause
    /// - return : the list of filtered text documents
    static func list(byPredicate predicate: NSPredicate?) -> [TextDocument] {
        var allResults: [TextDocument] = []

        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TextDocument")

        if let aPredicate = predicate {
            fetchRequest.predicate = aPredicate
        }

        TextDocument.queue.sync {
            do {
                if let context = CoreDataManager.managedContext() {
                    let results = try context.fetch(fetchRequest)

                    if let objects = results as? [TextDocument] {
                        allResults = objects
                    }
                }
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
            }
        }

        return allResults
    }

    /// Find text document by its server identifier (used to update a text document on a web service response)
    /// - parameters :
    ///   - identifiant : th server identifier for a text document
    /// - return : the text document found
    static func find(byIdServer identifiant: Int64) -> TextDocument? {
        let predicateFormat: String = String(format: "identifier = '%i'", identifiant)
        let predicate: NSPredicate = NSPredicate(format:predicateFormat)

        let textDocs: [TextDocument] = list(byPredicate: predicate)

        if textDocs.isEmpty {
            return nil
        }
        return textDocs[0]
    }
}
