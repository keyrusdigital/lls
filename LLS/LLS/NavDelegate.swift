//
//  NavDelegate.swift
//  LLS
//
//  Created by Gabriel Bremond on 04/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import Foundation
import UIKit

class NavDelegate: NSObject, UINavigationControllerDelegate {

    private let animator = Animator()

    func navigationController(_ navigationController: UINavigationController,
                              animationControllerFor operation: UINavigationControllerOperation,
                              from fromVC: UIViewController,
                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return animator
    }
}
