//
//  NavigationTableViewCell.swift
//  LLS
//
//  Created by Gabriel Bremond on 04/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import UIKit

class NavigationTableViewCell: UITableViewCell {

    @IBOutlet weak var previousButton: UIButton!

    @IBOutlet weak var nextButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
