//
//  WebserviceConsumer.swift
//  LLS
//
//  Created by Gabriel Bremond on 03/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import Foundation
import UIKit

enum URLType {
    case chapters   // Chapters URL type
    case lessons    // Lessons URL type
    case template   // Template URL type
}

class WebserviceConsummer: NSObject, NSURLConnectionDataDelegate {

    private let defaultSession = URLSession(configuration: URLSessionConfiguration.default)

    private let basedURL = Bundle.main.object(forInfoDictionaryKey: "WebserviceBasedURL") ?? ""

    /// Methods to retrieve chapters of a book.
    ///
    /// - Parameters:
    ///   - bookID: Identifier of the book.
    ///   - completionHandler: Block to execute on request response.
    func getElements(type: URLType, parentID: String,
                     completionHandler: @escaping (_ data: Data?) -> Void) {

        let completion = { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            var resultData: Data? = nil
            if let error = error { print("Error: \(error)")
            } else if let response = response as? HTTPURLResponse,
                let data = data {
                if response.statusCode == 200 {
                    resultData = data
                } else { print("HTTP error code: \(response.statusCode)")
                }
            }
            completionHandler(resultData)
        }

        // Test entries.
        guard let leadingURL = basedURL as? String else {
            let info = ["reason": "Invalid parameter WebserviceBasedURL in Info.plist"]
            let error = NSError(domain: "WebserviceConsumer", code: -1, userInfo: info)
            completion(nil, nil, error)
            return
        }
        if parentID.isEmpty {
            let info = ["reason": "Invalid identifier to retrive elements"]
            let error = NSError(domain: "WebserviceConsumer", code: -1, userInfo: info)
            completion(nil, nil, error)
            return
        }

        // Build URL string depending on type.
        var urlString = ""
        switch type {
        case .chapters:
            urlString = "\(leadingURL)books/\(parentID)/chapters"
            break
        case .lessons:
            urlString = "\(leadingURL)chapters/\(parentID)/lessons"
            break
        case .template:
            urlString = "\(leadingURL)templates/\(parentID)"
            break
        }

        // Build URL.
        guard let url = URL(string: urlString) else {
            let info = ["reason": "Unable to build URL"]
            let error = NSError(domain: "WebserviceConsumer", code: -1, userInfo: info)
            completion(nil, nil, error)
            return
        }

        // Prepare request.
        var request: URLRequest = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 5.0)
        request.httpMethod = "GET"
        let dataTask: URLSessionDataTask = defaultSession.dataTask(with: request, completionHandler: completion)

        // Run request
        dataTask.resume()
    }

    /// Methods to retrieve image from an URL.
    ///
    /// - Parameters:
    ///   - url: URL to load image from.
    ///   - completionHandler: Block to execute on request response.
    func downloadImage(url: URL, completion:@escaping (_ image: UIImage?) -> Void) {
        // Look into cache directory.
        if let data = Data.loadFromTemp(name: buildName(url.absoluteString)) {
            completion(UIImage(data: data))
            return
        }

        getDataFromUrl(url: url) { (data, _, error)  in
            guard let data = data, error == nil else {
                completion(nil)
                return
            }
            // Save into cache directory.
            data.saveToTemp(name: self.buildName(url.absoluteString))
            Thread.performOnMain {
                completion(UIImage(data: data))
            }
        }
    }

    // MARK: - Private methods

    private func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        guard let components = NSURLComponents(url: url, resolvingAgainstBaseURL: true) else {
            completion(nil, nil, nil)
            return
        }

        // Always use a secure connection.
        components.scheme = "https"

        guard let securedURL = components.url else {
            completion(nil, nil, nil)
            return
        }

        URLSession.shared.dataTask(with: securedURL) { (data, response, error) in
            completion(data, response, error)
            }.resume()
    }

    private func buildName(_ name: String) -> String {
        return name.replacingOccurrences(of: ":", with: "").replacingOccurrences(of: "/", with: "")
    }
}
