//
//  ChapterTests.swift
//  LLS
//
//  Created by Gabriel Bremond on 03/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import XCTest
@testable import LLS

class ChapterTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testSucess() {
        let aChapter = Chapter.create()
        aChapter.load(chapterDictionary: [:])
        XCTAssertNotNil(aChapter)

        aChapter.load(chapterDictionary: ["id": 42, "valid": true])
        XCTAssertNotNil(aChapter)
        XCTAssertTrue(aChapter.identifier == 42)
        XCTAssertTrue(aChapter.valid)
        XCTAssertNil(aChapter.title)
        XCTAssertNil(aChapter.url)

        aChapter.load(chapterDictionary: ["id": 42, "valid": true, "title": "Hi!", "url": "https://example.com"])
        XCTAssertNotNil(aChapter)
        XCTAssertTrue(aChapter.identifier == 42)
        XCTAssertTrue(aChapter.valid)
        XCTAssert(aChapter.title?.isEmpty == false)
        XCTAssert(aChapter.url?.isEmpty == false)
    }

}
