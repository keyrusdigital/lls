//
//  ChaptersTests.swift
//  LLS
//
//  Created by Gabriel Bremond on 03/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import XCTest
@testable import LLS

class ChaptersTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testSucess() {
        ChaptersHelper.load(data: Data())
        let chapters = Chapter.list()
        XCTAssertNotNil(chapters)
    }

}
