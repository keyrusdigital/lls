//
//  ImageDocumentTests.swift
//  LLS
//
//  Created by Gabriel Bremond on 04/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import XCTest
@testable import LLS

class ImageDocumentTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testSucess() {
        let aDocument = ImageDocument.create()
        aDocument.load(templateDictionary: [:])
        XCTAssertNotNil(aDocument)

        aDocument.load(templateDictionary: ["id": 42])
        XCTAssertNotNil(aDocument)
        XCTAssertTrue(aDocument.identifier == 42)

        aDocument.load(templateDictionary:
            ["id": 42, "url": "https://exemple.com", "credits": "@him", "caption": "Nice pic."])
        XCTAssertNotNil(aDocument)
        XCTAssertTrue(aDocument.identifier == 42)
        XCTAssert(aDocument.url?.isEmpty == false)
        XCTAssert(aDocument.credits?.isEmpty == false)
        XCTAssert(aDocument.caption?.isEmpty == false)
    }

}
