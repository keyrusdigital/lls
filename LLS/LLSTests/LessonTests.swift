//
//  LessonTests.swift
//  LLS
//
//  Created by Gabriel Bremond on 04/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import XCTest
@testable import LLS

class LessonTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testSucess() {
        let aLesson = Lesson.create()
        aLesson.load(lessonDictionary: [:])
        XCTAssertNotNil(aLesson)

        aLesson.load(lessonDictionary: ["id": 42, "valid": true])
        XCTAssertNotNil(aLesson)
        XCTAssertTrue(aLesson.identifier == 42)
        XCTAssertTrue(aLesson.valid)

        aLesson.load(lessonDictionary: ["id": 42, "valid": true, "page": 13, "title": "Hi!"])
        XCTAssertNotNil(aLesson)
        XCTAssertTrue(aLesson.identifier == 42)
        XCTAssertTrue(aLesson.valid)
        XCTAssert(aLesson.page == 13)
        XCTAssert(aLesson.title?.isEmpty == false)
    }

}
