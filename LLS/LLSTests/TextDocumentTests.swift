//
//  TextDocumentTests.swift
//  LLS
//
//  Created by Gabriel Bremond on 04/06/2017.
//  Copyright © 2017 photograve. All rights reserved.
//

import XCTest
@testable import LLS

class TextDocumentTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testSucess() {
        let aDocument = TextDocument.create()
        aDocument.load(templateDictionary: [:])
        XCTAssertNotNil(aDocument)

        aDocument.load(templateDictionary: ["id": 42])
        XCTAssertNotNil(aDocument)
        XCTAssertTrue(aDocument.identifier == 42)

        aDocument.load(templateDictionary: ["id": 42, "content": "Blabla bla"])
        XCTAssertNotNil(aDocument)
        XCTAssertTrue(aDocument.identifier == 42)
        XCTAssert(aDocument.content?.isEmpty == false)
    }

}
